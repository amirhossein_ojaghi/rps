from config import CHOICES, GAME_RULES
import random


def find_winner(user, system):

    res = {user, system}
    if len(res) == 1:
        return None

    return GAME_RULES[tuple(sorted(res))]


def user_choice():
    user = input("Enter your choice (r, p, s) : ")
    if user not in CHOICES:
        print("please enter a valid char : ")
        return user_choice()

    return user


def system_choice():
    system = random.choice(CHOICES)
    print(f"system choice is : {system}")
    return system


def play():
    result = {'user': 0, 'system': 0}

    while result['user'] < 3 and result['system'] < 3:

        user = user_choice()
        system = system_choice()
        winner = find_winner(user, system)

        if winner == user:
            print("You win")
            result['user'] += 1

        elif winner == system:
            print("You lose")
            result['system'] += 1

        else:
            print("Draw")


if __name__ == '__main__':
    play()
